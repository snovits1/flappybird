FLY! by Steven Novitske

Compatible for all iOS devices.

Tap and hold down to fly upward. Release to fall downward. Get too close to a bird and you lose. Simple game that gets challenging very quickly.

All animations are Steven Novitske originals except for the bird image.