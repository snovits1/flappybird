//
//  ViewController.m
//  p04-novitske
//
//  Created by Steven Novitske on 3/18/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//
//  Bird image location:
//  https://www.pinterest.com/pin/429108670718386277/

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *restartButton;
@property (weak, nonatomic) IBOutlet UILabel *clickToStart;
- (IBAction)restartGame:(id)sender;
@end

UILongPressGestureRecognizer* tap;
float dx;
float dy;
int screenWidth;
int screenHeight;
NSTimer* timer;
UIImageView* clouds1;
UIImageView* clouds2;
UIImageView* mount1;
UIImageView* mount2;
UIImageView* trees1;
UIImageView* trees2;
UIImageView* fly;
NSMutableArray* birds;
int numBirds;
int score;
bool tappedDown;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//Initialize variables
    dx = 1;
    dy = 0;
    numBirds = 1;
    score = 0;
    tappedDown = false;
    screenWidth = [UIScreen mainScreen].bounds.size.height;
    screenHeight = [UIScreen mainScreen].bounds.size.width;
    [self setBackground];
//Set up tap recognizer
    tap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(flyup)];
    tap.minimumPressDuration = 0;
    [self.view addGestureRecognizer:tap];
//Set up fly
    fly = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fly.png"]];
    fly.frame = CGRectMake(screenWidth/4, screenHeight/2, screenHeight/20, screenHeight/20);
    [self.view addSubview:fly];
//Set up birds array
    birds = [[NSMutableArray alloc] init];
    for(int i=0; i<3; i++) {
        UIImageView* bird = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"birds.png"]];
        bird.frame = CGRectMake(screenWidth+(screenWidth/2.5)*i, rand() % screenHeight, screenHeight/5, screenHeight/6);
        [birds addObject:bird];
        [self.view addSubview:bird];
    }
//Make sure labels are visible
    [self.view bringSubviewToFront:_scoreLabel];
    [self.view bringSubviewToFront:_clickToStart];
}


- (void)arrange {
    [self moveBackground];
    [self checkGameOver];
//Increase gravity if its less than 12
    if(dy < 15) dy += 1.5;
//Accelerate upward if tap is held down
    if(tappedDown && dy > -15) dy -= 3;
//Move fly up or down
    fly.center = CGPointMake(fly.center.x, fly.center.y + dy);
//If fly is out of screen, set it on the edge and set dy to 0
    if(fly.center.y-10 < 0) {
        fly.center = CGPointMake(fly.center.x, 10);
        dy = 0;
    } else if(fly.center.y+10 > screenHeight) {
        fly.center = CGPointMake(fly.center.x, screenHeight-10);
        dy = 0;
    }
//Increase score and difficulty
    score += dx;
    _scoreLabel.text = [NSString stringWithFormat:@"Score: %i",score];
    [self increaseDifficulty];
}

- (void)setBackground {
    clouds1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"clouds.png"]];
    clouds2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"clouds.png"]];
    mount1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mountains.png"]];
    mount2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mountains.png"]];
    trees1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"trees.png"]];
    trees2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"trees.png"]];
    
    clouds1.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    mount1.frame = clouds1.frame;
    trees1.frame = clouds1.frame;
    clouds2.frame = CGRectMake(screenWidth, 0, screenWidth, screenHeight);
    mount2.frame = clouds2.frame;
    trees2.frame = clouds2.frame;
    
    [self.view addSubview:clouds1];
    [self.view addSubview:clouds2];
    [self.view addSubview:mount1];
    [self.view addSubview:mount2];
    [self.view addSubview:trees1];
    [self.view addSubview:trees2];
}

- (void)moveBackground {
    clouds1.center = CGPointMake(clouds1.center.x - dx, clouds1.center.y);
    mount1.center = CGPointMake(mount1.center.x - 2*dx, mount1.center.y);
    trees1.center = CGPointMake(trees1.center.x - 4*dx, trees1.center.y);
    clouds2.center = CGPointMake(clouds2.center.x - dx, clouds1.center.y);
    mount2.center = CGPointMake(mount2.center.x - 2*dx, mount1.center.y);
    trees2.center = CGPointMake(trees2.center.x - 4*dx, trees1.center.y);
    if(clouds1.center.x <= -screenWidth/2) clouds1.frame = CGRectMake(clouds2.center.x + screenWidth/2, 0, screenWidth, screenHeight);
    if(clouds2.center.x <= -screenWidth/2) clouds2.frame = CGRectMake(clouds1.center.x + screenWidth/2, 0, screenWidth, screenHeight);
    if(mount1.center.x <= -screenWidth/2) mount1.frame = CGRectMake(mount2.center.x + screenWidth/2, 0, screenWidth, screenHeight);
    if(mount2.center.x <= -screenWidth/2) mount2.frame = CGRectMake(mount1.center.x + screenWidth/2, 0, screenWidth, screenHeight);
    if(trees1.center.x <= -screenWidth/2) trees1.frame = CGRectMake(trees2.center.x + screenWidth/2, 0, screenWidth, screenHeight);
    if(trees2.center.x <= -screenWidth/2) trees2.frame = CGRectMake(trees1.center.x + screenWidth/2, 0, screenWidth, screenHeight);
}

- (void)flyup {
    if(timer == NULL || ! timer.isValid) {
        timer = [NSTimer scheduledTimerWithTimeInterval:0.033 target:self selector:@selector(arrange) userInfo:nil repeats:YES];
        _clickToStart.text = @"";
    }
    if(tappedDown) tappedDown = false;
    else tappedDown = true;
}

- (void)checkGameOver {
    for(int i=0; i<3; i++) {
        UIImageView* bird = birds[i];
        bird.center = CGPointMake(bird.center.x - 5*dx, bird.center.y);
        if(bird.center.x + bird.bounds.size.width < 0) {
            bird.center = CGPointMake(screenWidth + bird.bounds.size.width/2, rand() % (screenHeight));
        }
        if(CGRectIntersectsRect(fly.frame, bird.frame)) {
            _clickToStart.text = @"Game Over";
            _clickToStart.textColor = [UIColor redColor];
            [timer invalidate];
            tap.enabled = NO;
            [self.view bringSubviewToFront:_restartButton];
        }
    }
}

- (void)increaseDifficulty {
    if(score > dx * dx * 300 && dx < 5) dx += .1;
}

- (IBAction)restartGame:(id)sender {
    dx = 1;
    dy = 0;
    numBirds = 1;
    score = 0;
    _scoreLabel.text = @"Score: 0";
    tappedDown = false;
    for(int i=0; i<3; i++) {
        UIImageView* bird = birds[i];
        bird.center = CGPointMake(screenWidth+(screenWidth/2.5)*(i+1), rand() % screenHeight);
    }
    fly.center = CGPointMake(screenWidth/4 + fly.bounds.size.width/2, screenHeight/2);
    _clickToStart.text = @"Click to Start";
    _clickToStart.textColor = [UIColor whiteColor];
    tap.enabled = YES;
    [self.view sendSubviewToBack:_restartButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
